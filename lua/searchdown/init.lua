local config = require("searchdown.config")
local M = {}

function M.setup(opts)
	config.setup(opts)
	require("searchdown.keymaps").create_keymaps()
end

return M
