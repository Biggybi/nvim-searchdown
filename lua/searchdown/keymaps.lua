local M = {}

local map = vim.keymap.set

local function line_forward()
	return vim.v.searchforward == 1
end

local char_forward = function()
	return vim.fn.getcharsearch()["forward"] == 1
end

local inline_keys = require("searchdown.config").options.inline_keys
local line_keys = require("searchdown.config").options.line_keys
local create_keymaps = require("searchdown.config").options.create_keymaps

-- vim.print(inline_keys)

function M.create_char_keymaps()
	map("n", inline_keys.next, function()
		return char_forward() and ";" or ","
	end, { expr = true })
	map("n", inline_keys.prev, function()
		return char_forward() and "," or ";"
	end, { expr = true })
	map("x", inline_keys.next, function()
		return char_forward() and ";" or ","
	end, { expr = true })
	map("x", inline_keys.prev, function()
		return char_forward() and "," or ";"
	end, { expr = true })
end

function M.create_line_keymaps()
	map("n", line_keys.next, function()
		return line_forward() and "n" or "N"
	end, { expr = true })
	map("n", line_keys.prev, function()
		return line_forward() and "N" or "n"
	end, { expr = true })
	map("x", line_keys.next, function()
		return line_forward() and "n" or "N"
	end, { expr = true })
	map("x", line_keys.prev, function()
		return line_forward() and "N" or "n"
	end, { expr = true })
end

function M.create_keymaps()
	if create_keymaps == false then
		return
	end
	M.create_char_keymaps()
	M.create_line_keymaps()
end

return M
