local M = {}

--- @class keys
--- @field next string
--- @field prev string

--- @class SearchDownOptions
--- @field inline_keys keys | nil
--- @field line_keys keys | nil
--- @field create_keymaps boolean | nil

---@type SearchDownOptions
local defaults = {
	inline_keys = { next = ";", prev = "," },
	line_keys = { next = "n", prev = "N" },
	create_keymaps = true,
}

---@param opts SearchDownOptions
function M.setup(opts)
	M.options = vim.tbl_deep_extend("force", {}, defaults, opts or {})
end

return M
